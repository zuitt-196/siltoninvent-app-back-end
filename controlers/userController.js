// CONTROLER FUINCTION USE AND IMPORT INTO ROUTE AS CALL BACK FUCNTION 
const asyncHandler = require('express-async-handler');
const User = require('../models/userModel');
const jwt = require('jsonwebtoken');
const bcrypt = require("bcryptjs");
const Token = require('../models/tokenModel')
const  crypto = require('crypto');
const  sendEmail = require('../utils/sendEmail')


// derfine to generate the token firts
const generateToken  =  (id) => {
         return jwt.sign({id}, process.env.JWT_SECRET, {expiresIn:"1d"})
        

};

// REGSITER USER SECTION CONTROLLER
const registerUser = asyncHandler(async (req, res) => {
    
    const {name, email, password} = req.body

    // VALIDATION  
    if (!name || !email || !password) {
        res.status(400);
        throw new Error('Please fill in all required fields');        
    };
    if (password.lenght < 6) {
        res.status(400);
        throw new Error('Password must be up to 6 characters');
    }

// check if user email already exists 
    const userExist = await User.findOne({email});
    if (userExist) {
        res.status(400);
        throw new Error('Email is already been used');       
    }


    // create new user 
    const user = await User.create({
        name,
        email,
        password,
    })

    
  // GENERATE TOKEN BY GETTING THE ID IN USER SDSDASDASDSADASDASDAS D SAD
  const token = generateToken(user._id)

  // SEND HTPP-only cookie
  res.cookie("token", token, {
    path: "/",
    httpOnly: true,
    expires: new Date(Date.now() + 1000 * 86400), // 1 day
    sameSite: "none",
    secure: true 
  })

    if (user) {
        const {_id, name, email, photo, phone, bio} = user
        res.status(201).json({
            _id, name, email, photo, phone, bio,token
        })
    }else{
        res.status(400)
        throw new Error('Invalid user data');
    }

})


// LOG IN USER SECTION CONTROLLER 

const loginUser = asyncHandler (async (req, res) => {
    const { email, password} = req.body;
    
    // validayte Request
    if (!email || !password) {
        res.status(400)
        throw new Error('Please add email and password');
    }

    // check if user exists
    const user = await User.findOne({email})

    // if the user emal is not exist
    if (!user) {
        res.status(400)
        throw new Error('User is not found, please singup');
    }

    // User exists, check if password is correctt and comapare it from database use bcrypt method 
    const passwordIsCorrect = await bcrypt.compare(password, user.password)

       
  // GENERATE TOKEN BY GETTING THE ID IN USER 
  const token = generateToken(user._id)

  // SEND HTPP-only cookie for traking the user acesss  for the front end
  if (passwordIsCorrect) {
    res.cookie("token", token, {
        path: "/",
        httpOnly: true,
        expires: new Date(Date.now() + 1000 * 86400), // 1 day
        sameSite: "none",
        secure: true 
      })
      
  }
  
    // if the user is exists
    if (user && passwordIsCorrect) {
        const {_id, name, email, photo, phone, bio} = user
        res.status(200).json({
            _id, name, email, photo, phone, bio, token
        })
    }else{
        res.status(400)
        throw new Error('Invalid/Password or email is incorrect') ;
    }
})

    
// LOG OUT USER FUNCTION CONTROLLER

const logOutUser = asyncHandler (async (req, res) => {

    res.cookie("token", "", {
        path: "/",
        httpOnly: true,
        expires: new Date(0), 
        sameSite: "none",
        secure: true 
      })

      return res.status(200).json({message: "SuccesFully Loggged Out"})
      
  
})

//  GET USER SECTION CONTROLLER
const getUser = asyncHandler (async (req, res) => {
    const user = await User.findById(req.user._id);

    // if user is found
    if (user) {
        const {_id, name, email, photo, phone, bio} = user
        res.status(201).json({
            _id, name, email, photo, phone, bio
        })
    }else{
        res.status(400)
        throw new Error('User not found');
    }
})


// GET lOGIN STATUS
const  loginStatus =  asyncHandler (async (req, res) => { 
    const token = req.cookies.token   
    
    if (!token) {
           return res.json(false);
    }

    // verify the token
    const verified = jwt.verify(token, process.env.JWT_SECRET);

    if (verified) {
            return res.json(true)
    }   

    return res.json(false)

    });


// UPDATE THE USER DATA CONTROLLER NOT INCLUDE THE PASSWORD
const updateUser = asyncHandler(async (req, res) => {
        // find  the  user id
       const user = await User.findById(req.user._id);
 
        //if the user is found and update it
        if (user) {
                const {name, email, photo, phone, bio} = user
                user.email = email;
                user.name = req.body.name || name;
                user.photo = req.body.photo || photo;
                user.phone = req.body.phone || phone;
                user.bio = req.body.bio || bio;

        // save the user update 
        const updateUser =  await user.save()
            res.status(200).json({
                  _id:updateUser._id ,
                  name: updateUser.name,
                  email:updateUser.email, 
                  photo: updateUser.photo,
                  phone: updateUser.phone,
                  bio: updateUser.bio
            })

        } else{
            res.status(404)
            throw new Error("Not User found")
        }
    });

 //  UPDATE OR CHGANGE THE PASSWORD USER DATA CONTROKLLER 
 const changePassword = asyncHandler(async (req, res) =>{
        // find  the  user id
       const user = await User.findById(req.user._id);
       // destrature the req.body
       const { oldpassword, password } = req.body;

        if (!user) {
                res.status(400);
                throw new Error('User is not found, Please signUp')
        }

       // validate or ,must be added it
       if (!oldpassword || !password) {
        res.status(400);
        throw new Error('Please and old and new Password')
       }

       // check if old password matches  password in databse 
       const passwordIsCorrect = await bcrypt.compare(oldpassword, user.password);

       // Save new password
       if (user && passwordIsCorrect) {
          user.password = password;
          await user.save();
          res.status(200).send("Password changed successfully")
       }else{
        res.status(400);
        throw new Error('Old password in incorrect')
       }

 })


 const forgotpassword = asyncHandler(async (req, res) => {
    const { email } = req.body;
    // console.log(email._id);
    const user = await User.findOne({ email })
    // console.log(user._id);
     

    // if the user is not exist
    if (!user) {
        res.status(404)
        throw new Error('User does not exist');        
    }


    //DELETE the token if it exists in database
    let token = await Token.findOne({userId:user._id})
    // if exist used delete it used mongoose deleteOne() method
    if (token) {
            await token.deleteOne();
    }

    // Craete Reset Token used the 32 string or character and convert into hex  with user id 
    let resetToken = crypto.randomBytes(32).toString("hex") + user._id
    console.log(resetToken);

    // hash token before saving to Db
    const hashedToken = crypto.createHash("sha256").update(resetToken).digest("hex")
    // console.log(hashToken);
    // res.send("For got pasword ")


    // Save token to DB
     await new Token({
        userId: user._id,
        token: hashedToken,
        createdAt:Date.now(),
        expiredAt: Date.now() + 30 * (60 * 1000) // 30 mintues expire this calcue for 30 minutes
    }).save()
    // save unto database used og save() method


    // Construct Reset URl in Front end
    const resetUrl = `${process.env.FRONTEND_URL}/resetpassword/${resetToken}`


    // Reset  Email
    const message = `<h2>${user.name}</h2>
                      <p>Plese use the url below tpo reset your password</p>
                      <p>This reset link in valid for only 30 minutes</p>
                        <a href=${resetUrl} clicktracking=off>${resetUrl}</a>

                        <p>Regard...</p>
                        <p>Silton Team</p>
                      ` 
    
                    
const subject = "Password Reset Request"
const send_to = user.email;
const send_from = process.env.EMAIL_USER


    try {
        await sendEmail(subject, message, send_to,send_from)
            // it send from front end
        res.status(200).json({success:true , message:"Reset Email Sent"})
    } catch (error) {
        res.status(500)
        throw new Error("Email not sent, please try again")
    }
 })



 // RESET PASSWORD FUNCTION CONTROLLER  AND CREATE NEW PASSOWRD 
const resetPassword  = asyncHandler (async (req, res) => {
    const {password} = req.body
    const {resetToken} = req.params

      // hash token, then compare to Token in DB
      const hashedToken = crypto.createHash("sha256").update(resetToken).digest("hex")


    // find token in DB
    const userToken = await Token.findOne({
        token: hashedToken,
        expiredAt: {
             $gt: Date.now()
        }
    })
    // console.log(userToken.expiredAt);
    if (!userToken) {
            res.status(404)
            throw new Error("Invalid or Expired Token")
    }

    // find user by use the id
    const user = await User.findOne({_id: userToken.userId})
    user.password = password
    await user.save()
    res.status(200).json({message: "Password Reset Successful, please Log in"})

})








module.exports = {
    registerUser,
    loginUser,
    logOutUser,
    getUser,
    loginStatus,
    updateUser,
    changePassword,
    forgotpassword,
    resetPassword
}


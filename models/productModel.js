const mongoose = require('mongoose');


const productScheme = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId, // refer tht id in mongoDb 
        required: true,
        ref:"User"
    },

    name: {
        type: String,
        required: [true, "Please add a name"],
        trim: true

    },
    sku:{
        type: String,
        required: true,
        default: "SKU",
        trim: true
    },
    category:{
        type: String,
        required: [true, "Please add a category"],
        trim: true
    },
    quantity:{
        type: String,
        required: [true, "Please add a quantity"],
        trim: true
    },
    price:{
        type: String,
        required: [true, "Please add a price"],
        trim: true
    },
    desc:{
        type: String,
        required: [true, "Please add a description"],
        trim: true
    },

    image:{
        type: Object,
        default: {}
    },
    

},
{   

    timestamps: true,
})



const Product = mongoose.model('Product', productScheme);

module.exports = Product
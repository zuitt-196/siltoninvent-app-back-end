const mongoose = require('mongoose');


const tokenScheme = mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId, // refer tht id in mongoDb 
        required: true,
        ref:"user"
    },
    token :{
        type:String,
        required: true
    },
    createdAt :{
        type:Date,
        required: true
    },
    expiredAt:{
        type:Date,
        required: true
    }

})



const Token = mongoose.model('token', tokenScheme);

module.exports = Token
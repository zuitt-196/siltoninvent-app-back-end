const nodemailer = require('nodemailer');

// allow to send email meessgae from your server 
const sendEmail = async (subject, message, send_to, send_from, reply_to) => {
    // create Email transporter
    const transporter = nodemailer.createTransport({
        host:process.env.EMAIL_HOST,
        port: 587,
        auth:{
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASS
        },
        tls:{
            rejectUnauthorized:false
        }
    })

    // Option for sending email which is define if we try send for some else other emal
    const option = {
        from: send_from,
        to: send_to,
        replyTo: reply_to,
        subject: subject,
        html:message
    }

    // send email
    transporter.sendMail(option, function (err,info){
         if (err) {
            console.log(err);
         }else{
            //  console.log(info);

         }

    })
    }


 module.exports = sendEmail   
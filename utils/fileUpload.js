const multer  = require('multer')


// Defin file storage
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads')
    },

    filename: function (req, file, cb) {
    
      cb(null, new Date().toISOString().replace(/:/g, "-")  + "-" + file.originalname) // 07/01/2023 gave unique like an id but this path name of image every file  upload 
    }
  })
  

  // Specify file format that can be saved
 function fileFilter (req, file, cb) {

        // definer what are type of files that we uplaod jpg, png or jpeg
    if ( file.mimetype === "image/png" || file.mimetype === "image/jpg" || file.mimetype === "image/jpeg" ) {

            // To accept the file pass `true`, like so: or executing of resolve 
        cb(null, true)
    }else{
            // To reject this file pass `false`, like so: ir executing of reject
         cb(null, false)
  
    }
  


  
  }
  // upload it ,, further detailes according the EE6 if the property and value are same is no need to define it instead we use as one.
  const upload = multer({ storage, fileFilter })


  // File Size Formatter 
  const fileSizeFormatter = (bytes, decimal) => {
    if (bytes === 0) {
            return "0 Bytes"
    }

    const dm = decimal || 2;
    const sizes = [ "Bytes", "KB", "MB", "GB", "TB", "EB"," YB", "ZB"];
    const index = Math.floor(Math.log(bytes) / Math.log(1000))
    return (parseFloat((bytes / Math.pow(1000, index)).toFixed(dm))) + " " + sizes[index]
  }

  module.exports = {
    upload,
    fileSizeFormatter
  }